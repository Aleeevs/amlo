AMLO identity ontology
=======================

The AMLO-id is an upper experimental ontology for the [AMLO project](https://gitlab.com/mopso/amlo) that defines some concepts 
related *Identity* in the AML domain.

*AMLO-id* is technically expressed in the RDF/OWL - the language of the World Wide Web for machine readable inference processing. The RDF/OWL file can be embedded in computer applications that can perform logic functions such as inferring classifications and relationships.

*AMLO-id* can be used as an upper ontology to derive taxonomies and axioms as well as a way to exchange information among investigators and tools.

The IRI of the ontology is *http://w3id.org/amlo/id*

You can explore the *AMLO-id* by:

- editing the [RDF source file](id.rdf) serialized using XML/RDF or with [turtle](id.ttl); 
- loading the AMLO-core IRI with [Standford University Protégeè](https://protege.stanford.edu/) ;
- browsing the html documentation generated by [LODE](https://w3id.org/lode/http://w3id.org/amlo/id) ;
- visualizing the whole *AMLO-core* with [WebOwl](http://visualdataweb.de/webvowl/#iri=http://visualdataweb.de/webvowl/#iri=http://w3id.org/amlo/id) 



License and Credits
--------------------

*AMLO-id*  is Copyright (c) by 2019-2020 by LinkedData.Center as part of the Mopso project.

*AMLO-id*  is released under the [Creative Commons Attribution 4.0 license](LICENSE)

**NOTE: this module is under active development. Please contact authors if you plan to use in production.**

